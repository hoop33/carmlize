package com.taxonic.carml.engine;

import java.io.OutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class StreamingModel implements Model {
  private static final long serialVersionUID = 1L;
  private OutputStream out;

  private Resource lastSubject = null;
  private Model buffer = new LinkedHashModel();

  private int maxSubjectsToBuffer;
  private int subjectCount = 0;

  public StreamingModel(OutputStream out) {
    this(out, 4096);
  }

  public StreamingModel(OutputStream out, int maxSubjectsToBuffer) {
    this.out = out;
    this.maxSubjectsToBuffer = maxSubjectsToBuffer;
  }

  @Override
  public boolean add(Resource subj, IRI pred, Value obj, Resource... contexts) {
    if (lastSubject != null && subj != lastSubject) {
      subjectCount++;
      if(subjectCount >= maxSubjectsToBuffer) {
        flush();
        subjectCount = 0;
      }
    }

    return buffer.add(subj, pred, obj, contexts);
  }

  public void flush() {
    if (!buffer.isEmpty()) {
      Rio.write(buffer, out, RDFFormat.TURTLE);
      buffer.clear();
    }
  }

  @Override
  public int size() {
    return 0;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public boolean contains(Resource subj, IRI pred, Value obj, Resource... contexts) {
    return false;
  }

  @Override
  public boolean add(Statement e) {
    return false;
  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public Iterator<Statement> iterator() {
    return null;
  }

  @Override
  public Object[] toArray() {
    return null;
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(Collection<? extends Statement> c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {
  }

  @Override
  public Set<Namespace> getNamespaces() {
    return null;
  }

  @Override
  public Model unmodifiable() {
    return null;
  }

  @Override
  public void setNamespace(Namespace namespace) {
  }

  @Override
  public Optional<Namespace> removeNamespace(String prefix) {
    return null;
  }

  @Override
  public boolean clear(Resource... context) {
    return false;
  }

  @Override
  public boolean remove(Resource subj, IRI pred, Value obj, Resource... contexts) {
    return false;
  }

  @Override
  public Model filter(Resource subj, IRI pred, Value obj, Resource... contexts) {
    return null;
  }

  @Override
  public Set<Resource> subjects() {
    return null;
  }

  @Override
  public Set<IRI> predicates() {
    return null;
  }

  @Override
  public Set<Value> objects() {
    return null;
  }
}
