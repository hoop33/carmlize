package com.grailbox.carmlize;

import com.taxonic.carml.engine.StreamingRmlMapper;
import com.taxonic.carml.logical_source_resolver.CsvResolver;
import com.taxonic.carml.logical_source_resolver.JsonPathResolver;
import com.taxonic.carml.logical_source_resolver.XPathResolver;
import com.taxonic.carml.model.TriplesMap;
import com.taxonic.carml.util.RmlMappingLoader;
import com.taxonic.carml.vocab.Rdf;
import org.eclipse.rdf4j.rio.RDFFormat;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.Normalizer;
import java.util.Set;

public class App {
  private final String mappingFile;

  public App(String... args) {
    mappingFile = args[0];
  }

  public void run() throws FileNotFoundException {
    Set<TriplesMap> mapping = RmlMappingLoader.build().load(RDFFormat.TURTLE, new FileInputStream(mappingFile));
    StreamingRmlMapper mapper = StreamingRmlMapper.newBuilder()
      .setLogicalSourceResolver(Rdf.Ql.Csv, new CsvResolver())
      .setLogicalSourceResolver(Rdf.Ql.JsonPath, new JsonPathResolver())
      .setLogicalSourceResolver(Rdf.Ql.XPath, new XPathResolver())
      .fileResolver(FileSystems.getDefault().getPath("."))
      .iriUnicodeNormalization(Normalizer.Form.NFKC)
      .iriUpperCasePercentEncoding(false)
      .build();

    long start = System.currentTimeMillis();
    mapper.map(mapping, new BufferedOutputStream(System.out));
    System.out.printf("execution time: %d ms%n", (System.currentTimeMillis() - start));
  }

  private static void showInstructions() {
    System.out.println("Usage: java -jar carmlize.jar <rml file>");
  }

  public static void main(String[] args) throws Exception {
    if (args.length == 0) {
      showInstructions();
    } else {
      new App(args).run();
    }
  }
}
