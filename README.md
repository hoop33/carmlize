# Carmlize

> A CLI around carml

## Building

```console
$ mvn package
```

## Usage

```console
$ java -jar target/carmlize-1.0-SNAPSHOT-jar-with-dependencies.jar <rml file>
```

**Note:** writes output to `stdout`.

Last line of output shows execution time.
